﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_5
{
    class Admin
    {
        List<Admin> _admins = new List<Admin>();
        public string _name;
        public string _surname;
        public int _age;

        public Admin(string Name, string Surname, int Age)
        {
            _name = Name;
            _surname = Surname;
            _age = Age;
        }

        public void Add_Admin(string Name, string Surname, int _age)
        {
            Admin client = new Admin(Name, Surname, _age);
            _admins.Add(client);
        }
    }
}
