﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_5
{
    class Client
    {
        List<Client> _clients = new List<Client>();
        private string _name;
        private string _surname;
        private int _age;
        private double _balance;

        public Client(string Name, string Surname, int Age, double Balance)
        {
            _name = Name;
            _surname = Surname;
            _age = Age;
            _balance = Balance;
        }

        public void Add_Client(string Name, string Surname, int Age, double Balance)
        {
            Client client = new Client(Name, Surname, Age, Balance);
            _clients.Add(client);
        }

        public void Show_Client()
        {
            string SearchName = Console.ReadLine();

            for (int i = 0; i < _clients.Count; i++)
            {
                if (SearchName == _clients[i]._name)
                {
                    Console.WriteLine("====================");
                    Console.WriteLine("Имя клиента: " + _clients[i]._name);
                    Console.WriteLine("Фамилия клиента: " + _clients[i]._surname);
                    Console.WriteLine("Возраст клиента: " + _clients[i]._age);
                    Console.WriteLine("Баланс клиента: " + _clients[i]._balance);
                }
            }
        }
    }
}
