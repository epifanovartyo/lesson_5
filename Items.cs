﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_5
{
    class Items : Shop, IShowVegetables, IShowUnVegetables, IShowPhones, IShowTv
    {
        public string _name;
        public string _data;
        public float _price;
        public int _countProducts;
        public string _classProducts;

        public Items(string Name, string Data, float Price, string ClassProducts, int CountProducts)
        {
            _name = Name;
            _data = Data;
            _price = Price;
            _classProducts = ClassProducts;
            _countProducts = CountProducts;
        }


        public void ShowVegetables()
        {
            string search_class = "Вегетарианское";

            for (int i = 0; i < _product.Count; i++)
            {
                if (search_class == _product[i]._classProducts)
                {
                    Console.WriteLine("=================");
                    Console.WriteLine("Название продукта: " + _product[i]._name);
                    Console.WriteLine("Дата производства продукта: " + _product[i]._data);
                    Console.WriteLine("Стоимость продукта: " + _product[i]._price);
                    Console.WriteLine("Количество продуктов: " + _product[i]._countProducts);
                    Console.WriteLine("Определение товара: " + _product[i]._classProducts);
                }
            }
        }

        public void ShowUnVegetables()
        {
            string search_class = "Не Вегетарианское";

            for (int i = 0; i < _product.Count; i++)
            {
                if (search_class == _product[i]._classProducts)
                {
                    Console.WriteLine("=================");
                    Console.WriteLine("Название продукта: " + _product[i]._name);
                    Console.WriteLine("Дата производства продукта: " + _product[i]._data);
                    Console.WriteLine("Стоимость продукта: " + _product[i]._price);
                    Console.WriteLine("Количество продуктов: " + _product[i]._countProducts);
                    Console.WriteLine("Определение товара: " + _product[i]._classProducts);
                }
            }
        }

        public void ShowPhones()
        {
            string search_class = "Телефон";

            for (int i = 0; i < _product.Count; i++)
            {
                if (search_class == _product[i]._classProducts)
                {
                    Console.WriteLine("=================");
                    Console.WriteLine("Название продукта: " + _product[i]._name);
                    Console.WriteLine("Дата производства продукта: " + _product[i]._data);
                    Console.WriteLine("Стоимость продукта: " + _product[i]._price);
                    Console.WriteLine("Количество продуктов: " + _product[i]._countProducts);
                    Console.WriteLine("Определение товара: " + _product[i]._classProducts);
                }
            }
        }

        public void ShowTv()
        {
            string search_class = "Телевизор";

            for (int i = 0; i < _product.Count; i++)
            {
                if (search_class == _product[i]._classProducts)
                {
                    Console.WriteLine("=================");
                    Console.WriteLine("Название продукта: " + _product[i]._name);
                    Console.WriteLine("Дата производства продукта: " + _product[i]._data);
                    Console.WriteLine("Стоимость продукта: " + _product[i]._price);
                    Console.WriteLine("Количество продуктов: " + _product[i]._countProducts);
                    Console.WriteLine("Определение товара: " + _product[i]._classProducts);
                }
            }
        }
    }
}
public interface IShowVegetables
{
    void ShowVegetables();
}

public interface IShowUnVegetables
{
    void ShowUnVegetables();
}

public interface IShowPhones
{
    void ShowPhones();
}

public interface IShowTv
{
    void ShowTv();
}