﻿using System;
using System.Collections.Generic;

namespace Lesson_5
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Admin _admin = new Admin("", "", 0);
            Client _client = new Client("", "", 0, 0);
            Items _products = new Items("", "", 0, "", 0);
            _products.Add_Products("Морковка", "02.06.2021 г.", 2500, "Вегетарианское", 10);
            _products.Add_Products("Огурец", "11.10.2021 г.", 1700, "Вегетарианское", 10);
            _products.Add_Products("Свинина", "04.05.2022 г.", 1500, "Не Вегетарианское", 10);
            _products.Add_Products("Курица", "02.09.2022 г.", 4500, "Не Вегетарианское", 10);
            _products.Add_Products("Айфон 11", "01.01.2021 г.", 10000, "Телефон", 15);
            _products.Add_Products("Андроид", "07.11.2021 г.", 5000, "Телефон", 150);
            _products.Add_Products("LG 12L3rt", "01.12.2019 г.", 1900, "Телевизор", 40);
            _products.Add_Products("Samsung 5WE67u", "05.06.2020 г.", 2700, "Телевизор", 35);

            Console.WriteLine("============================");
            Console.WriteLine("Как вы желаете зайти в онлайн магазин:\n" + "1.Как покупатель.\n" + "2.Как работник.\n");
            int answer = Convert.ToInt32(Console.ReadLine());

            while (true)
            {
                switch (answer)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Введите имя клиента:");
                        string _name = Console.ReadLine();
                        Console.WriteLine("Введите фамилию клиента:");
                        string _surname = Console.ReadLine();
                        Console.WriteLine("Введите возраст клиента:");
                        int Age = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Введите баланс клиента:");
                        double _balance = Convert.ToDouble(Console.ReadLine());
                        _client.Add_Client(_name, _surname, Age, _balance);

                        while (true)
                        {
                            Console.Clear();
                            Console.WriteLine("============================");
                            Console.WriteLine("Здравствуйте " + _name + ", мы рады вас видеть в нашем магазине " + _products._name_shop + ", мы находимся по адресу " + _products._address_shop + " !!!");
                            Console.WriteLine("Выберите категорию товаров:\n");
                            Console.WriteLine("1.Продукты.\n" + "2.Техника.\n" + "3.Информация о клиенте.\n");
                            int choice = Convert.ToInt32(Console.ReadLine());

                            switch (choice)
                            {
                                case 1:
                                    Console.Clear();
                                    Console.WriteLine("Выберите отдел продуктов:\n" + "1.Вегетарианские продукты.\n" + "2.Мясные продукты.\n");
                                    int choice_1 = Convert.ToInt32(Console.ReadLine());

                                    if (choice_1 == 1)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("У нас имеется следующий ассортимент товаров:\n");
                                        _products.ShowVegetables();
                                        Console.WriteLine("=================");
                                        Console.WriteLine("Желаете вернутся в меню?");
                                        string answer_1 = Console.ReadLine();

                                        if (answer_1 == "Да" || answer_1 == "да")
                                        {
                                            continue;
                                        }
                                        return;

                                    }

                                    if (choice_1 == 2)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("У нас имеется следующий ассортимент товаров:\n");
                                        _products.ShowUnVegetables();
                                        Console.WriteLine("=================");
                                        Console.WriteLine("Желаете вернутся в меню?");
                                        string answer_1 = Console.ReadLine();

                                        if (answer_1 == "Да" || answer_1 == "да")
                                        {
                                            continue;
                                        }
                                        return;
                                    }
                                    break;

                                case 2:
                                    Console.Clear();
                                    Console.WriteLine("Выберите отдел техники:\n" + "1.Телефоны.\n" + "2.Телевизоры.\n");
                                    int choice_2 = Convert.ToInt32(Console.ReadLine());

                                    if (choice_2 == 1)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("У нас имеется следующий ассортимент товаров:\n");
                                        _products.ShowPhones();
                                        Console.WriteLine("=================");
                                        Console.WriteLine("Желаете вернутся в меню?");
                                        string answer_1 = Console.ReadLine();

                                        if (answer_1 == "Да" || answer_1 == "да")
                                        {
                                            continue;
                                        }
                                        return;

                                    }

                                    if (choice_2 == 2)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("У нас имеется следующий ассортимент товаров:\n");
                                        _products.ShowTv();
                                        Console.WriteLine("=================");
                                        Console.WriteLine("Желаете вернутся в меню?");
                                        string answer_1 = Console.ReadLine();

                                        if (answer_1 == "Да" || answer_1 == "да")
                                        {
                                            continue;
                                        }
                                        return;
                                    }
                                    break;

                                case 3:
                                    Console.Clear();
                                    Console.WriteLine("Введите имя клиента:");
                                    _client.Show_Client();
                                    Console.WriteLine("=================");
                                    Console.WriteLine("Желаете вернутся в меню?");
                                    string answer_2 = Console.ReadLine();

                                    if (answer_2 == "Да" || answer_2 == "да")
                                    {
                                        continue;
                                    }
                                    return;
                            }
                            break;
                        }
                        break;

                    case 2:
                        Console.Clear();
                        Console.WriteLine("Введите имя администратора:");
                        string Name_1 = Console.ReadLine();
                        Console.WriteLine("Введите фамилию администратора:");
                        string Surname_1 = Console.ReadLine();
                        Console.WriteLine("Введите возраст администратора:");
                        int Age_1 = Convert.ToInt32(Console.ReadLine());
                        _admin.Add_Admin(Name_1, Surname_1, Age_1);

                        while (true)
                        {
                            Console.Clear();
                            Console.WriteLine("============================");
                            Console.WriteLine("Здравствуйте администратор " + Name_1 + ", выберите функцию работы с магазином:\n" + "1.Добавить товар.\n" + "2.Удалить товар.\n" + "3.Показать полный список товаров.\n" + "4.Показать список товаров по цене.");
                            int Сhoice = Convert.ToInt32(Console.ReadLine());

                            switch (Сhoice)
                            {
                                case 1:
                                    Console.Clear();
                                    Console.WriteLine("Введите имя товара:");
                                    string name_product = Console.ReadLine();
                                    Console.WriteLine("Введите дату производства товара:");
                                    string data_product = Console.ReadLine();
                                    Console.WriteLine("Введите цену товара:");
                                    int price_product = Convert.ToInt32(Console.ReadLine());
                                    Console.WriteLine("Ввыберите класс товара:\n" + "1.Вегетарианское.\n" + "2.Не Вегетарианское.\n" + "3.Телефон.\n" + "4.Телевизор.\n");
                                    int choice_class = Convert.ToInt32(Console.ReadLine());
                                    string class_product = null;

                                    switch (choice_class)
                                    {
                                        case 1:
                                            class_product = "Вегетарианское";
                                            break;
                                        case 2:
                                            class_product = "Не Вегетарианское";
                                            break;
                                        case 3:
                                            class_product = "Телефон";
                                            break;
                                        case 4:
                                            class_product = "Телефизор";
                                            break;
                                    }
                                    
                                    Console.WriteLine("Введите кол-во товара:");
                                    int count_product = Convert.ToInt32(Console.ReadLine());
                                    _products.Add_Products(name_product, data_product, price_product, class_product, count_product);
                                    Console.WriteLine("Товар успешно добавлен!!!");
                                    Console.WriteLine("=================");
                                    Console.WriteLine("Желаете вернутся в меню?");
                                    string answer_2 = Console.ReadLine();

                                    if (answer_2 == "Да" || answer_2 == "да")
                                    {
                                        continue;
                                    }
                                    return;

                                case 2:
                                    Console.Clear();
                                    Console.WriteLine("Введите имя товара:");
                                    _products.Remove_Product();
                                    Console.WriteLine("Товар успешно удалён!!!");
                                    Console.WriteLine("=================");
                                    Console.WriteLine("Желаете вернутся в меню?");
                                    string answer_3 = Console.ReadLine();

                                    if (answer_3 == "Да" || answer_3 == "да")
                                    {
                                        continue;
                                    }
                                    return;

                                case 3:
                                    Console.Clear();
                                    _products.ShowAllProducts();
                                    Console.WriteLine("=================");
                                    Console.WriteLine("Желаете вернутся в меню?");
                                    string answer_4 = Console.ReadLine();

                                    if (answer_4 == "Да" || answer_4 == "да")
                                    {
                                        continue;
                                    }
                                    return;

                                case 4:
                                    Console.Clear();
                                    _products.ShowAllProducts_Price();
                                    Console.WriteLine("=================");
                                    Console.WriteLine("Желаете вернутся в меню?");
                                    string answer_5 = Console.ReadLine();

                                    if (answer_5 == "Да" || answer_5 == "да")
                                    {
                                        continue;
                                    }
                                    return;
                            }
                            break;
                        }break;
                }
            }
        }
    }
}

