﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_5
{
    abstract class Shop
    {
        protected List<Items> _product = new List<Items>();
        public string _name_shop = "<Копейка>";
        public string _address_shop = "Ул.Ленина Д.56б";

        public void Add_Products(string Name, string Data, float Price, string ClassProducts, int CountProducts)
        {
            Items products = new Items(Name, Data, Price, ClassProducts, CountProducts);
            _product.Add(products);
        }

        public void Remove_Product()
        {
            string SearchName = Console.ReadLine();

            for (int i = 0; i < _product.Count; i++)
            {
                if (SearchName == _product[i]._name)
                {
                    _product.Remove(_product[i]);
                }
            }

        }

        public void ShowAllProducts_Price()
        {
            _product.Sort((left, right) => right._price.CompareTo(left._price));
            foreach (Items products in _product)
            {
                Console.WriteLine("=================");
                Console.WriteLine("Название продукта: " + products._name);
                Console.WriteLine("Дата производства продукта: " + products._data);
                Console.WriteLine("Стоимость продукта: " + products._price);
                Console.WriteLine("Количество продуктов: " + products._countProducts);
                Console.WriteLine("Определение товара: " + products._classProducts);
            }
        }

        public void ShowAllProducts()
        {
            for (int i = 0; i < _product.Count; i++)
            {
                Console.WriteLine("=================");
                Console.WriteLine("Название продукта: " + _product[i]._name);
                Console.WriteLine("Дата производства продукта: " + _product[i]._data);
                Console.WriteLine("Стоимость продукта: " + _product[i]._price);
                Console.WriteLine("Количество продуктов: " + _product[i]._countProducts);
                Console.WriteLine("Определение товара: " + _product[i]._classProducts);
            }
        }
    }
}
